#!/bin/bash

if [ "$#" -ne 1 ]; then
	echo "Usage $0 PORT_NUMBER"
	exit 1
fi

echo -ne "\xFF\xFF\xFF\xFFTSource Engine Query\x00" | nc -w1 -u $(hostname --ip-address) "$1" | grep -q ^ && exit 0 || exit 1
